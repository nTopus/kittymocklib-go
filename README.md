# KittyMock go driver

https://gitlab.com/nTopus/kitty-mock

Esta biblioteca abstrai a comunicação http com o kitty-mock e os clientes podem acessar os seus recursos como se fossem um processo local.

Além de expor uma interface é dado uma implementação para rodar um _http server_ local para testes unitários.

## Criando um Mocker

KittyMock

```golang
mocker, err := kitty_mock.NewMocker("kittymock.com", 6999)
```

Local _Http Server_

```golang
mocker := http_server.NewMocker()
```

Um mocker implementa a interface **_Mocker_** e pode: 
* Verificar a comunicação com o servidor.
* Criar rotas http.
* Criar rotas websocket. (não implementado)
* Pegar a lista de rotas criadas.
* Pega endereço onde o _mocker_ foi instanciado.
* Fechar completamente a conexão do _mocker_. (parando de receber solicitações em qualquer rota criada)

```golang
type Mocker interface {
    HealthCheck() error
    CreateHttpRoute(method, path, responseBody string, responseStatus int, validator ...Validator) (HttpRoute, error)
    CreateWebSocketRoute(path string) (WebSocketRoute, error)
    GetHttpRoutes() ([]HttpRouteDetail, error)
    Addr() net.Addr
    Delete() error
}
```

## Criando uma rota http

```golang
// criando uma rota sem validação
httpRoute, err := mocker.CreateHttpRoute("GET", "/route/to/path", "ok", "200")
```

Uma rota http implementa a interface **_HttpRoute_** e pode:
* Pegar o histórico de solicitações recebidas.
* Limpar o histórico.
* Remover esta rota. (novas solicitações gerarão 404)
* Atualizar esta rota. (apenas retorno e validações)
* Pegar os detalhes. (praticamente as configurações de criação)
* Enviar uma requisição para esta rota. (este é um helper para que cria e envia uma requisição http para esta rota) **[remover este método]** 

```golang
type HttpRoute interface {
	GetHistory() (requestLog []ClientRequest, err error)
	ClearHistory() error
	Delete() error
	Update(responseBody string, responseStatus int, validators ...Validator) error
	Details() (detail HttpRouteDetail, err error)
	Send(body []byte, header http.Header) (*http.Response, error)
}
```

### Pegando o histórico desta rota

Primeiramente deve ser enviado uma requisição.

```golang
// fazendo uma requisição GET (sem header e sem body)
resp, err := http.Get("/route/to/path")

// resp.StatusCode == 200
// resp.Body == "ok" (body é um io.Reader então este valor será acessado apenas após sua leitura)
```

Os valores de _StatusCode_ e do _Body_ são os mesmos configurados na criação do **_HttpRoute_**

```golang
requestLog, err := httpRoute.GetHistory()

// requestLog[0].IP     == "192.168.2.10"
// requestLog[0].Header == nil                                               (na verdade o header vem preenchido com os valores default da requisição que o pacote http do go preenche, porém como não passamos nenhum header explicitamente vamos considerar nessa documentação que este valor é nulo)  
// requestLog[0].Body   == ""                                                (numa requisição GET normalmente não é enviado nenhum body)
// requestLog[0].Method == "GET"
// requestLog[0].Url    == "/route/to/path"
// requestLog[0].Date   == "Wed Jan 29 2020 11:28:02 GMT-0300 (GMT-03:00)"
```