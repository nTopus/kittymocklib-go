package kittymocklib_go

import (
	"fmt"
	"github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"github.com/onsi/gomega/types"
	"net/http"
	"strings"
)

func ContainDetail(route, validator, response types.GomegaMatcher) types.GomegaMatcher {
	return gomega.ContainElement(HaveDetail(route, validator, response))
}

func HaveDetail(route, validator, response types.GomegaMatcher) types.GomegaMatcher {
	return gstruct.MatchAllFields(gstruct.Fields{
		"Filters":   route,
		"Validator": validator,
		"Response":  response,
	})
}

func HaveResponse(body string, code int) types.GomegaMatcher {
	return gstruct.MatchAllFields(gstruct.Fields{
		"Code": gomega.BeEquivalentTo(code),
		"Body": gomega.BeEquivalentTo(body),
	})
}

func HaveRoute(method, path string) types.GomegaMatcher {
	return gstruct.MatchAllFields(gstruct.Fields{
		"Path":   gomega.BeEquivalentTo(path),
		"Method": gomega.BeEquivalentTo(method),
	})
}

func ContainValidator(matchBody string, matchHeader http.Header, responseBody string, responseStatus int) types.GomegaMatcher {
	return gomega.ContainElement(gstruct.MatchAllFields(gstruct.Fields{
		"Matchers": gstruct.MatchAllFields(gstruct.Fields{
			"Header": ParseHeaderMatcher(matchHeader),
			"Body":   gomega.BeEquivalentTo(matchBody),
		}),
		"Response": gstruct.MatchAllFields(gstruct.Fields{
			"Code": gomega.BeEquivalentTo(responseStatus),
			"Body": gomega.BeEquivalentTo(responseBody),
		}),
	}))
}

func ContainClientRequest(method, path, body string, header http.Header) types.GomegaMatcher {
	return gomega.ContainElement(HaveClientRequest(method, path, body, header))
}

func HaveClientRequest(method, path, body string, header http.Header) types.GomegaMatcher {
	return gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
		"Body":   gomega.BeEquivalentTo(body),
		"Method": gomega.BeEquivalentTo(method),
		"Header": ParseHeaderMatcher(header),
		"Url":    gomega.BeEquivalentTo(path),
	})
}

func ParseHeaderMatcher(header http.Header) types.GomegaMatcher {
	var headerMatcher []types.GomegaMatcher
	for key, values := range header {
		headerValues := strings.Join(values, ", ")
		headerKeyLower := strings.ToLower(key)
		var strAux []string
		for _, str := range strings.Split(key, "-") {
			strAux = append(strAux, fmt.Sprintf("%s%s", strings.ToUpper(string(str[0])), str[1:]))
		}
		headerKeyCamel := strings.Join(strAux, "-")
		if len(headerValues) == 0 {
			matcherWithCamelCase := gomega.HaveKeyWithValue(headerKeyCamel, gomega.Not(gomega.BeEmpty()))
			matcherWithLowerCase := gomega.HaveKeyWithValue(headerKeyLower, gomega.Not(gomega.BeEmpty()))
			headerMatcher = append(headerMatcher, gomega.Or(matcherWithLowerCase, matcherWithCamelCase))
		} else {
			matcherWithCamelCase := gomega.HaveKeyWithValue(headerKeyCamel, headerValues)
			matcherWithLowerCase := gomega.HaveKeyWithValue(headerKeyLower, headerValues)
			headerMatcher = append(headerMatcher, gomega.Or(matcherWithLowerCase, matcherWithCamelCase))
		}
	}
	return gomega.And(headerMatcher...)
}

func HaveLenAnd(length int, and ...types.GomegaMatcher) types.GomegaMatcher {
	allMatcher := gomega.And(gomega.HaveLen(length))
	for _, matcher := range and {
		allMatcher = gomega.And(allMatcher, matcher)
	}
	return allMatcher
}

func FailWithRouteDoesNotExist() types.GomegaMatcher {
	return gomega.MatchError("404 - route does not exist")
}

func FailWithRouteAlreadyCreated() types.GomegaMatcher {
	return gomega.MatchError("400 - route already created in this mocker")
}
