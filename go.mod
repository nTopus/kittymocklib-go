module gitlab.com/nTopus/kittymocklib-go

go 1.14

require (
	github.com/onsi/gomega v1.10.1
	github.com/pkg/errors v0.9.1
)
