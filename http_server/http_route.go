package http_server

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/client"
	"gitlab.com/nTopus/kittymocklib-go/http_client"
	"net/http"
)

func NewHttpRoute(host, method, path, responseBody string, responseStatus int, validator []client.Validator, mocker *Mocker) *Route {
	return &Route{host: host, method: method, path: path, responseBody: responseBody, responseStatus: responseStatus, mocker: mocker, validators: validator}
}

type Route struct {
	mocker         *Mocker
	host           string
	method         string
	path           string
	responseBody   string
	responseStatus int
	deleted        bool
	validators     []client.Validator
	history        []client.ClientRequest
}

func (r *Route) GetHistory() (requestLog []client.ClientRequest, err error) {
	r.mocker.Lock()
	defer r.mocker.Unlock()
	if err := r.isDeleted(); err != nil {
		return requestLog, err
	}
	return r.history, nil
}

func (r *Route) ClearHistory() error {
	r.mocker.Lock()
	defer r.mocker.Unlock()
	if err := r.isDeleted(); err != nil {
		return err
	}
	r.history = nil
	return nil
}

func (r *Route) Delete() error {
	r.mocker.Lock()
	defer r.mocker.Unlock()
	if err := r.isDeleted(); err != nil {
		return err
	}
	r.deleted = true
	r.mocker.deleteRoute(r)
	return nil
}

func (r *Route) Update(responseBody string, responseStatus int, validator ...client.Validator) error {
	r.mocker.Lock()
	defer r.mocker.Unlock()
	if err := r.isDeleted(); err != nil {
		return err
	}
	r.responseBody = responseBody
	r.responseStatus = responseStatus
	r.validators = validator
	return nil
}

func (r *Route) Details() (detail client.HttpRouteDetail, err error) {
	r.mocker.Lock()
	defer r.mocker.Unlock()
	if err := r.isDeleted(); err != nil {
		return detail, err
	}
	return r.createDetail(), nil
}

func (r *Route) createDetail() client.HttpRouteDetail {
	return client.HttpRouteDetail{
		Filters: client.Filters{
			Path:   r.path,
			Method: r.method,
		},
		Validator: r.validators,
		Response: client.Response{
			Code: r.responseStatus,
			Body: r.responseBody,
		},
	}
}

func (r *Route) Send(body []byte, header http.Header) (*http.Response, error) {
	return http_client.SendRequest(r.method, fmt.Sprintf("http://%s%s", r.host, r.path), body, header)
}

func (r *Route) isDeleted() error {
	if r.deleted {
		return errors.New("404 - route does not exist")
	}
	return nil
}

func (r *Route) addHistory(requestLog client.ClientRequest) {
	r.history = append(r.history, requestLog)
}

func (r *Route) hasValidator() bool {
	return len(r.validators) > 0
}

func (r *Route) getResponse(ip, body string, header http.Header) (string, int) {
	if r == nil {
		return notFound()
	}
	r.mocker.Lock()
	defer r.mocker.Unlock()
	r.addHistory(client.NewClientRequest(ip, r.method, r.path, body, header))
	if !r.hasValidator() {
		return r.responseBody, r.responseStatus
	}
	for _, validator := range r.validators {
		if validator.Matches(body) {
			return validator.Body, validator.Code
		}
	}
	return notFound()
}

func notFound() (string, int) {
	return http.StatusText(http.StatusNotFound), http.StatusNotFound
}
