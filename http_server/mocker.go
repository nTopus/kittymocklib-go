package http_server

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/client"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptest"
	"sync"
)

func NewMocker(port int) (*Mocker, error) {
	mocker := &Mocker{routes: &Routes{}}
	server := httptest.NewServer(mocker)
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, errors.Wrap(err, "failed to listen to port")
	}
	server = httptest.NewUnstartedServer(mocker)
	server.Listener = listener
	server.Start()
	mocker.server = server
	return mocker, nil
}

type Mocker struct {
	routes  *Routes
	server  *httptest.Server
	deleted bool
	sync.Mutex
}

func (m *Mocker) CreateWebSocketRoute(path string) (client.WebSocketRoute, error) {
	if err := m.isDeleted(); err != nil {
		return nil, err
	}
	panic("implement me")
}

func (m *Mocker) HealthCheck() error {
	if err := m.isDeleted(); err != nil {
		return err
	}
	return nil
}

func (m *Mocker) CreateHttpRoute(method, path, responseBody string, responseStatus int, validator ...client.Validator) (client.HttpRoute, error) {
	if err := m.isDeleted(); err != nil {
		return nil, err
	}
	if _, ok := m.routes.Get(method, path); ok {
		return nil, errors.New("400 - route already created in this mocker")
	}
	route := NewHttpRoute(m.server.Listener.Addr().String(), method, path, responseBody, responseStatus, validator, m)
	m.routes.Add(route)
	return route, nil
}

func (m *Mocker) GetHttpRoutes() ([]client.HttpRouteDetail, error) {
	if err := m.isDeleted(); err != nil {
		return nil, err
	}
	var details []client.HttpRouteDetail
	m.routes.ForEach(func(route *Route) {
		details = append(details, route.createDetail())
	})
	return details, nil
}

func (m *Mocker) Delete() error {
	if err := m.isDeleted(); err != nil {
		return err
	}
	m.Lock()
	defer m.Unlock()
	m.deleted = true
	m.server.Close()
	return nil
}

func (m *Mocker) Addr() net.Addr {
	m.Lock()
	defer m.Unlock()
	return m.server.Listener.Addr()
}

func (m *Mocker) ServeHTTP(httpResponse http.ResponseWriter, httpRequest *http.Request) {
	defer httpRequest.Body.Close()
	body, _ := ioutil.ReadAll(httpRequest.Body)
	responseBody, responseStatus :=
		m.getRoute(httpRequest.Method, httpRequest.URL.RequestURI()).
			getResponse(httpRequest.RemoteAddr, string(body), httpRequest.Header)
	httpResponse.WriteHeader(responseStatus)
	_, _ = httpResponse.Write([]byte(responseBody))
}

func (m *Mocker) getRoute(method, path string) *Route {
	r, _ := m.routes.Get(method, path)
	return r
}

func (m *Mocker) deleteRoute(route *Route) {
	m.routes.Del(route.method, route.path)
}

func (m *Mocker) isDeleted() error {
	m.Lock()
	defer m.Unlock()
	if m.deleted {
		return errors.New("404 - mocker does not exist")
	}
	return nil
}
