package http_server

import (
	"fmt"
	"sync"
)

type Routes struct {
	m sync.Map
}

func (r *Routes) Add(route *Route) {
	r.m.Store(getKey(route.method, route.path), route)
}

func (r *Routes) Get(method string, path string) (*Route, bool) {
	value, ok := r.m.Load(getKey(method, path))
	if !ok {
		return nil, ok
	}
	return value.(*Route), ok
}

func (r *Routes) Del(method string, path string) {
	r.m.Delete(getKey(method, path))
}

type ForEachFn func(*Route)

func (r *Routes) ForEach(fn ForEachFn) {
	r.m.Range(func(key, value interface{}) bool {
		fn(value.(*Route))
		return true
	})
}

func getKey(method string, path string) string {
	return fmt.Sprintf("%s%s", method, path)
}
