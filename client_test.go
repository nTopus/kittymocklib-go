package kittymocklib_go

import (
	"fmt"
	"net/http"
	"testing"

	. "github.com/onsi/gomega"
	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/client"
	"gitlab.com/nTopus/kittymocklib-go/http_client"
	"gitlab.com/nTopus/kittymocklib-go/http_server"
	"gitlab.com/nTopus/kittymocklib-go/kitty_mock"
)

func Test_Create_A_Mocker(t *testing.T) {
	g := NewGomegaWithT(t)
	t.Run("Successfully create kitty mocker", func(t *testing.T) {
		mocker, err := kitty_mock.NewMocker("kittymock.com.br", 6999, kitty_mock.Parameters{})
		g.Expect(err).ShouldNot(HaveOccurred())
		AssertMocker(t, mocker)
	})
	t.Run("Successfully create http server mocker", func(t *testing.T) {
		mocker, err := http_server.NewMocker(6998)
		g.Expect(err).ShouldNot(HaveOccurred())
		AssertMocker(t, mocker)
	})
}

func Test_Create_A_Mocker_In_Specific_Port(t *testing.T) {
	g := NewGomegaWithT(t)
	t.Run("Should request a create mocker in a specific port", func(t *testing.T) {
		mocker, err := kitty_mock.NewMocker("kittymock.com.br", 6999, kitty_mock.Parameters{Port: 7001})
		g.Expect(err).ShouldNot(HaveOccurred())
		AssertMocker(t, mocker)
	})
	t.Run("In case of the invalid range should receive a specific fail for out of range", func(t *testing.T) {
		invalidPort := 9999
		_, err := kitty_mock.NewMocker("kittymock.com.br", 6999, kitty_mock.Parameters{Port: invalidPort})
		g.Expect(err).Should(HaveOccurred())
		g.Expect(err.Error()).Should(BeIdenticalTo("failed to parse create mocker response: 500 - port is out of range"))
	})
}

func AssertMocker(t *testing.T, mocker client.Mocker) {
	g := NewGomegaWithT(t)
	t.Run("Get empty http routes before create http route", func(t *testing.T) {
		g.Expect(mocker.GetHttpRoutes()).Should(BeEmpty())
	})
	t.Run("Successfully health check", func(t *testing.T) {
		g.Expect(mocker.HealthCheck()).Should(Succeed())
	})
	t.Run("Successfully create http route", func(t *testing.T) {
		path := "/path"
		method := "GET"
		responseBody := "response-body"
		responseCode := 200
		route, err := mocker.CreateHttpRoute(method, path, responseBody, responseCode)
		g.Expect(err).ShouldNot(HaveOccurred())
		t.Run("Do not create same http route", func(t *testing.T) {
			_, err := mocker.CreateHttpRoute(method, path, responseBody, responseCode)
			g.Expect(errors.Cause(err)).Should(FailWithRouteAlreadyCreated())
		})
		t.Run("Get route on the list", func(t *testing.T) {
			g.Expect(mocker.GetHttpRoutes()).Should(HaveLenAnd(1, ContainDetail(HaveRoute(method, path), BeEmpty(), HaveResponse(responseBody, responseCode))))
		})
		t.Run("Get empty history before send request", func(t *testing.T) {
			g.Expect(route.GetHistory()).Should(BeEmpty())
		})
		t.Run("Clear history before send request should not fail", func(t *testing.T) {
			g.Expect(route.ClearHistory()).Should(Succeed())
		})
		t.Run("Get details with created configuration", func(t *testing.T) {
			g.Expect(route.Details()).Should(HaveDetail(HaveRoute(method, path), BeEmpty(), HaveResponse(responseBody, responseCode)))
		})
		t.Run("Successfully send request on a created http route", func(t *testing.T) {
			clientBody := []byte("client-body")
			clientHeader := http.Header{"X-Client-Header": {"client", "header"}}
			httpResponse, err := route.Send(clientBody, clientHeader)
			g.Expect(err).ShouldNot(HaveOccurred())
			g.Expect(httpResponse).Should(HaveHTTPStatus(responseCode))
			g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(responseBody))
			t.Run("Get history with client request", func(t *testing.T) {
				g.Expect(route.GetHistory()).Should(HaveLenAnd(1, ContainClientRequest(method, path, string(clientBody), clientHeader)))
				t.Run("Successfully clear history", func(t *testing.T) {
					g.Expect(route.ClearHistory()).Should(Succeed())
					t.Run("Get empty history", func(t *testing.T) {
						g.Expect(route.GetHistory()).Should(BeEmpty())
					})
				})
			})
			max := 10
			t.Run(fmt.Sprintf("Successfully send %d requests", max), func(t *testing.T) {
				t.Run("Sending synchronously", func(t *testing.T) {
					for i := 0; i < max; i++ {
						httpResponse, err := route.Send(clientBody, clientHeader)
						g.Expect(err).ShouldNot(HaveOccurred())
						g.Expect(httpResponse).Should(HaveHTTPStatus(responseCode))
						g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(responseBody))
					}
				})
				t.Run(fmt.Sprintf("Get history with %d elements", max), func(t *testing.T) {
					g.Expect(route.GetHistory()).Should(HaveLen(max))
					t.Run("Successfully clear history", func(t *testing.T) {
						g.Expect(route.ClearHistory()).Should(Succeed())
					})
				})
				t.Run("Sending asynchronously", func(t *testing.T) {
					for i := 0; i < max; i++ {
						go func() {
							httpResponse, err := route.Send(clientBody, clientHeader)
							g.Expect(err).ShouldNot(HaveOccurred())
							g.Expect(httpResponse).Should(HaveHTTPStatus(responseCode))
							g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(responseBody))
						}()
					}
					t.Run(fmt.Sprintf("Get history with %d elements", max), func(t *testing.T) {
						g.Eventually(func() []client.ClientRequest {
							v, _ := route.GetHistory()
							return v
						}, "2s").Should(HaveLen(max))
						t.Run("Successfully clear history", func(t *testing.T) {
							g.Expect(route.ClearHistory()).Should(Succeed())
						})
					})
				})
			})
		})
		t.Run("Successfully update a http route", func(t *testing.T) {
			newResponseBody := "updated-body"
			newResponseCode := 201
			g.Expect(route.Update(newResponseBody, newResponseCode)).Should(Succeed())
			t.Run("Successfully send request on an updated http route", func(t *testing.T) {
				clientBody := []byte("client-body")
				clientHeader := http.Header{"X-Client-Header": {"client", "header"}}
				httpResponse, err := route.Send(clientBody, clientHeader)
				g.Expect(err).ShouldNot(HaveOccurred())
				g.Expect(httpResponse).Should(HaveHTTPStatus(newResponseCode))
				g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(newResponseBody))
				t.Run("Successfully clear history", func(t *testing.T) {
					g.Expect(route.ClearHistory()).Should(Succeed())
					t.Run("Get empty history", func(t *testing.T) {
						g.Expect(route.GetHistory()).Should(BeEmpty())
					})
				})
			})
			t.Run("Get details with updated configuration", func(t *testing.T) {
				g.Expect(route.Details()).Should(HaveDetail(HaveRoute(method, path), BeEmpty(), HaveResponse(newResponseBody, newResponseCode)))
			})
		})
		t.Run("Successfully delete a http route", func(t *testing.T) {
			g.Expect(route.Delete()).Should(Succeed())
			t.Run("Page not found on a nonexistent http route", func(t *testing.T) {
				g.Expect(route.Send(nil, nil)).Should(HaveHTTPStatus(http.StatusNotFound))
			})
			t.Run("Do not get details of a nonexistent http route", func(t *testing.T) {
				_, err := route.Details()
				g.Expect(errors.Cause(err)).Should(FailWithRouteDoesNotExist())
			})
			t.Run("Do not get history of a nonexistent http route", func(t *testing.T) {
				_, err := route.GetHistory()
				g.Expect(errors.Cause(err)).Should(FailWithRouteDoesNotExist())
			})
			t.Run("Do not clear history of a nonexistent http route", func(t *testing.T) {
				g.Expect(errors.Cause(route.ClearHistory())).Should(FailWithRouteDoesNotExist())
			})
			t.Run("Do not update a nonexistent http route", func(t *testing.T) {
				g.Expect(errors.Cause(route.Update("", 200))).Should(FailWithRouteDoesNotExist())
			})
			t.Run("Do not delete a nonexistent http route", func(t *testing.T) {
				g.Expect(errors.Cause(route.Delete())).Should(FailWithRouteDoesNotExist())
			})
			t.Run("Get empty http route list", func(t *testing.T) {
				g.Expect(mocker.GetHttpRoutes()).Should(BeEmpty())
			})
		})
	})
	t.Run("Successfully create http route with validation", func(t *testing.T) {
		method := "GET"
		path := "/path/with/validation"
		body := []byte("valid-body")
		var header http.Header
		responseBody := "ok"
		responseStatus := 202
		validator := client.WithValidator(string(body), header, responseBody, responseStatus)
		route, err := mocker.CreateHttpRoute(method, path, "", 0, validator)
		g.Expect(err).ShouldNot(HaveOccurred())
		t.Run("Successfully send a request", func(t *testing.T) {
			httpResponse, err := route.Send(body, header)
			g.Expect(err).ShouldNot(HaveOccurred())
			g.Expect(httpResponse).Should(HaveHTTPStatus(responseStatus))
			g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(responseBody))
			t.Run("Successfully get history", func(t *testing.T) {
				g.Expect(route.GetHistory()).Should(HaveLen(1))
				g.Expect(route.ClearHistory()).Should(Succeed())
			})
		})
		t.Run("Get details with validator configuration", func(t *testing.T) {
			g.Expect(route.Details()).Should(HaveDetail(HaveRoute(method, path), ContainValidator(string(body), header, responseBody, responseStatus), HaveResponse("", 0)))
		})
		t.Run("Get route on the list", func(t *testing.T) {
			g.Expect(mocker.GetHttpRoutes()).Should(HaveLenAnd(1, ContainDetail(HaveRoute(method, path), ContainValidator(string(body), header, responseBody, responseStatus), HaveResponse("", 0))))
		})
		t.Run("Page not found with invalid inputs", func(t *testing.T) {
			g.Expect(route.Send([]byte("not-valid-body"), nil)).Should(HaveHTTPStatus(http.StatusNotFound))
			t.Run("Successfully get history", func(t *testing.T) {
				g.Expect(route.GetHistory()).Should(HaveLen(1))
				g.Expect(route.ClearHistory()).Should(Succeed())
			})
		})
		t.Run("Successfully update http route with validation", func(t *testing.T) {
			updatedBody := []byte("update-valid-body")
			newResponseBody := "new-response"
			newResponseStatus := 201
			g.Expect(route.Update("", 0, client.WithValidator(string(updatedBody), header, newResponseBody, newResponseStatus))).Should(Succeed())
			t.Run("Successfully send a request", func(t *testing.T) {
				httpResponse, err := route.Send(updatedBody, header)
				g.Expect(err).ShouldNot(HaveOccurred())
				g.Expect(httpResponse).Should(HaveHTTPStatus(newResponseStatus))
				g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(newResponseBody))
			})
			t.Run("Page not found with invalid inputs (valid before update)", func(t *testing.T) {
				g.Expect(route.Send(body, header)).Should(HaveHTTPStatus(http.StatusNotFound))
			})
			t.Run("Get details with updated validator", func(t *testing.T) {
				g.Expect(route.Details()).Should(HaveDetail(HaveRoute(method, path), ContainValidator(string(updatedBody), header, newResponseBody, newResponseStatus), HaveResponse("", 0)))
			})
		})
		t.Run("Successfully delete a http route", func(t *testing.T) {
			g.Expect(route.Delete()).Should(Succeed())
		})
	})
	t.Run("Successfully create http route with query", func(t *testing.T) {
		path := "/path?param1=param1&param2=param2"
		method := "GET"
		responseBody := "response-body"
		responseCode := 200
		route, err := mocker.CreateHttpRoute(method, path, responseBody, responseCode)
		g.Expect(err).ShouldNot(HaveOccurred())
		t.Run("Successfully send a request", func(t *testing.T) {
			clientBody := []byte("client-body")
			clientHeader := http.Header{"X-Client-Header": {"client", "header"}}
			httpResponse, err := route.Send(clientBody, clientHeader)
			g.Expect(err).ShouldNot(HaveOccurred())
			g.Expect(httpResponse).Should(HaveHTTPStatus(responseCode))
			g.Expect(http_client.GetBody(httpResponse)).Should(BeEquivalentTo(responseBody))
			t.Run("Successfully get history", func(t *testing.T) {
				g.Expect(route.GetHistory()).Should(HaveLenAnd(1, ContainClientRequest(method, path, string(clientBody), clientHeader)))
				g.Expect(route.ClearHistory()).Should(Succeed())
			})
		})
		t.Run("Successfully get details", func(t *testing.T) {
			g.Expect(route.Details()).Should(HaveDetail(HaveRoute(method, path), BeEmpty(), HaveResponse(responseBody, responseCode)))
		})
		t.Run("Successfully update route", func(t *testing.T) {
			g.Expect(route.Update("", responseCode)).Should(Succeed())
		})
		t.Run("Successfully delete a http route", func(t *testing.T) {
			g.Expect(route.Delete()).Should(Succeed())
		})
	})
	max := 10
	t.Run(fmt.Sprintf("Successfully create %d http routes", max), func(t *testing.T) {
		for i := 0; i < max; i++ {
			_, err := mocker.CreateHttpRoute("GET", fmt.Sprintf("/path/%d", i), "ok", 200)
			g.Expect(err).ShouldNot(HaveOccurred())
		}
		t.Run(fmt.Sprintf("Get %d http routes", max), func(t *testing.T) {
			g.Expect(mocker.GetHttpRoutes()).Should(HaveLen(max))
		})
	})
	t.Run("Successfully delete a mocker", func(t *testing.T) {
		g.Expect(mocker.Delete()).Should(Succeed())
		t.Run("Do not delete an nonexistent mocker", func(t *testing.T) {
			g.Expect(mocker.Delete()).ShouldNot(Succeed())
		})
		t.Run("Do not health check on a nonexistent mocker", func(t *testing.T) {
			g.Expect(mocker.HealthCheck()).ShouldNot(Succeed())
		})
		t.Run("Do not create http route on a nonexistent mocker", func(t *testing.T) {
			_, err := mocker.CreateHttpRoute("", "", "", 200)
			g.Expect(err).Should(HaveOccurred())
		})
		t.Run("Do not create websocket route on a nonexistent mocker", func(t *testing.T) {
			_, err := mocker.CreateWebSocketRoute("")
			g.Expect(err).Should(HaveOccurred())
		})
		t.Run("Do not get http routes on a nonexistent mocker", func(t *testing.T) {
			_, err := mocker.GetHttpRoutes()
			g.Expect(err).Should(HaveOccurred())
		})
	})
}
