package kitty_mock

import (
	"fmt"
	"net"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/client"
	"gitlab.com/nTopus/kittymocklib-go/http_client"
)

type Parameters struct {
	Port int
}

func NewMocker(host string, port int, parameters Parameters) (*Mocker, error) {
	httpResponse, err := http_client.SendJsonRequest(http.MethodPost, fmt.Sprintf("http://%s:%d/create?port=%d", host, port, parameters.Port), nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create mocker")
	}
	var createResponse struct {
		Port int
	}
	err = http_client.ParseJsendData(httpResponse, &createResponse)
	return &Mocker{host: host, port: createResponse.Port}, errors.Wrap(err, "failed to parse create mocker response")
}

type Mocker struct {
	host string
	port int
}

func (m *Mocker) CreateHttpRoute(method, path, responseBody string, responseStatus int, validators ...client.Validator) (client.HttpRoute, error) {
	path = addSlashOnFirstCharacter(path)
	data := map[string]interface{}{
		"filters": map[string]interface{}{
			"path":   path,
			"method": method,
		},
		"validator": validators,
		"response": map[string]interface{}{
			"code": responseStatus,
			"body": responseBody,
		},
	}
	httpResponse, err := http_client.SendJsonRequest(http.MethodPost, fmt.Sprintf("http://%s:%d/=^.^=/route", m.host, m.port), data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create http route")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return NewHttpRoute(m.host, m.port, method, path), errors.Wrap(err, "failed to parse create http route response")
}

func (m *Mocker) CreateWebSocketRoute(path string) (client.WebSocketRoute, error) {
	path = addSlashOnFirstCharacter(path)
	data := map[string]interface{}{
		"filters": map[string]interface{}{
			"path": path,
		},
	}
	httpResponse, err := http_client.SendJsonRequest(http.MethodPost, fmt.Sprintf("http://%s:%d/=^.^=/ws-route", m.host, m.port), data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create web socket route")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return NewWebSocketRoute(m.host, m.port, path), errors.Wrap(err, "failed to parse create web socket route response")
}

func (m *Mocker) HealthCheck() error {
	httpResponse, err := http_client.SendJsonRequest(http.MethodGet, fmt.Sprintf("http://%s:%d/", m.host, m.port), nil)
	if err != nil {
		return errors.Wrap(err, "failed to check health")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse health check response")
}

func (m *Mocker) GetHttpRoutes() (details []client.HttpRouteDetail, err error) {
	httpResponse, err := http_client.SendJsonRequest(http.MethodGet, fmt.Sprintf("http://%s:%d/=^.^=/routes", m.host, m.port), nil)
	if err != nil {
		return details, errors.Wrap(err, "failed to get http routes")
	}
	err = http_client.ParseJsendData(httpResponse, &details)
	return details, errors.Wrap(err, "failed to parse get http routes response")
}

func (m *Mocker) Delete() error {
	httpResponse, err := http_client.SendJsonRequest(http.MethodDelete, fmt.Sprintf("http://%s:%d/", m.host, m.port), nil)
	if err != nil {
		return errors.Wrap(err, "failed to delete mocker")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse delete mocker response")
}

type addr struct {
	network, addr string
}

func (a addr) Network() string {
	return a.network
}

func (a addr) String() string {
	return a.addr
}

func (m *Mocker) Addr() net.Addr {
	return addr{network: "http", addr: fmt.Sprintf("%s:%d", m.host, m.port)}
}

func addSlashOnFirstCharacter(str string) string {
	if strings.Index(str, "/") == 0 {
		return str
	}
	return fmt.Sprintf("/%s", str)
}
