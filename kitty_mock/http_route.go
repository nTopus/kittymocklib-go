package kitty_mock

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/client"
	"gitlab.com/nTopus/kittymocklib-go/http_client"
	"net/http"
	"net/url"
)

func NewHttpRoute(host string, port int, method, path string) *HttpRoute {
	return &HttpRoute{host: host, port: port, method: method, path: path}
}

type HttpRoute struct {
	host   string
	port   int
	method string
	path   string
}

func (r *HttpRoute) GetHistory() (requestLog []client.ClientRequest, err error) {
	httpResponse, err := http_client.SendJsonRequest(http.MethodGet, fmt.Sprintf("http://%s:%d/=^.^=/history?path=%s&method=%s", r.host, r.port, url.QueryEscape(r.path), r.method), nil)
	if err != nil {
		return requestLog, errors.Wrap(err, "failed to get history")
	}
	err = http_client.ParseJsendData(httpResponse, &requestLog)
	return requestLog, errors.Wrap(err, "failed to parse get history response")
}

func (r *HttpRoute) ClearHistory() error {
	httpResponse, err := http_client.SendJsonRequest(http.MethodDelete, fmt.Sprintf("http://%s:%d/=^.^=/history?path=%s&method=%s", r.host, r.port, url.QueryEscape(r.path), r.method), nil)
	if err != nil {
		return errors.Wrap(err, "failed to clear history")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse clear history response")
}

func (r *HttpRoute) Delete() error {
	httpResponse, err := http_client.SendJsonRequest(http.MethodDelete, fmt.Sprintf("http://%s:%d/=^.^=/route?path=%s&method=%s", r.host, r.port, url.QueryEscape(r.path), r.method), nil)
	if err != nil {
		return errors.Wrap(err, "failed to delete http route")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse delete http route response")
}

func (r *HttpRoute) Update(responseBody string, responseStatus int, validator ...client.Validator) error {
	data := map[string]interface{}{
		"response": map[string]interface{}{
			"code": responseStatus,
			"body": responseBody,
		},
		"validator": validator,
	}
	httpResponse, err := http_client.SendJsonRequest(http.MethodPatch, fmt.Sprintf("http://%s:%d/=^.^=/route?path=%s&method=%s", r.host, r.port, url.QueryEscape(r.path), r.method), data)
	if err != nil {
		return errors.Wrap(err, "failed to update route")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse update http route response")
}

func (r *HttpRoute) Details() (detail client.HttpRouteDetail, err error) {
	httpResponse, err := http_client.SendJsonRequest(http.MethodGet, fmt.Sprintf("http://%s:%d/=^.^=/route?path=%s&method=%s", r.host, r.port, url.QueryEscape(r.path), r.method), nil)
	if err != nil {
		return detail, errors.Wrap(err, "failed to get details http route")
	}
	err = http_client.ParseJsendData(httpResponse, &detail)
	return detail, errors.Wrap(err, "failed to parse get details http route response")
}

func (r *HttpRoute) Send(body []byte, header http.Header) (*http.Response, error) {
	return http_client.SendRequest(r.method, fmt.Sprintf("http://%s:%d%s", r.host, r.port, r.path), body, header)
}
