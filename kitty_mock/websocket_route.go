package kitty_mock

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/nTopus/kittymocklib-go/http_client"
	"net/http"
)

func NewWebSocketRoute(host string, port int, path string) *WebSocketRoute {
	return &WebSocketRoute{host: host, port: port, path: path}
}

type WebSocketRoute struct {
	host string
	port int
	path string
}

func (w *WebSocketRoute) Delete() error {
	httpResponse, err := http_client.SendJsonRequest(http.MethodDelete, fmt.Sprintf("http://%s:%d/=^.^=/route?path=%s", w.host, w.port, w.path), nil)
	if err != nil {
		return errors.Wrap(err, "failed to delete web socket route")
	}
	err = http_client.ParseJsendData(httpResponse, nil)
	return errors.Wrap(err, "failed to parse delete web socket route response")
}
