package http_client

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"net/http"
)

func SendJsonRequest(method, url string, data interface{}) (*http.Response, error) {
	body, err := json.Marshal(data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to encode request")
	}
	return SendRequest(method, url, body, nil)
}

func SendRequest(method, url string, body []byte, header http.Header) (*http.Response, error) {
	request, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create request")
	}
	request.Header = header
	httpResponse, err := http.DefaultClient.Do(request)
	return httpResponse, errors.Wrap(err, "failed to send request")
}
