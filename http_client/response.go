package http_client

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
)

type JSend struct {
	Status  string
	Data    interface{}
	Message string
}

func ParseJsendData(httpResponse *http.Response, data interface{}) error {
	body, err := GetBody(httpResponse)
	if err != nil {
		return err
	}
	if httpResponse.StatusCode == 204 {
		return nil
	}
	if data == nil {
		data = struct{}{}
	}
	var jsend JSend
	jsend.Data = &data
	err = json.Unmarshal(body, &jsend)
	if err != nil {
		return errors.Wrap(err, "failed to parse jsend")
	}
	if jsend.Status != "success" || httpResponse.StatusCode >= 400 {
		return errors.Errorf("%d - %s", httpResponse.StatusCode, jsend.Message)
	}
	return nil
}

func GetBody(httpResponse *http.Response) ([]byte, error) {
	defer httpResponse.Body.Close()
	body, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read body")
	}
	return body, nil
}
