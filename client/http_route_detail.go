package client

type HttpRouteDetail struct {
	Filters   Filters     `json:"filters"`
	Validator []Validator `json:"validator"`
	Response  Response    `json:"response"`
}
