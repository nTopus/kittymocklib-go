package client

type Filters struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}
