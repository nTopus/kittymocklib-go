package client

type Response struct {
	Code int    `json:"code"`
	Body string `json:"body"`
}
