package client

import (
	"net/http"
)

type HttpRoute interface {
	GetHistory() (requestLog []ClientRequest, err error)
	ClearHistory() error
	Delete() error
	Update(responseBody string, responseStatus int, validators ...Validator) error
	Details() (detail HttpRouteDetail, err error)
	Send(body []byte, header http.Header) (*http.Response, error)
}
