package client

import (
	"net/http"
	"strings"
	"time"
)

func NewClientRequest(ip, method, path, body string, header http.Header) ClientRequest {
	return ClientRequest{IP: ip, Header: readHeader(header), Body: body, Method: method, Url: path, Date: time.Now().Format(time.RFC1123)}
}

type ClientRequest struct {
	IP     string
	Header map[string]string
	Body   string
	Method string
	Url    string
	Date   string
}

func readHeader(httpHeader http.Header) map[string]string {
	header := map[string]string{}
	for key, values := range httpHeader {
		header[key] = strings.Join(values, ", ")
	}
	return header
}
