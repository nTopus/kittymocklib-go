package client

import (
	"net/http"
	"strings"
)

func WithValidator(body string, header http.Header, responseBody string, responseStatus int) Validator {
	headerMatcher := map[string]string{}
	for key, values := range header {
		headerMatcher[key] = strings.Join(values, ", ")
	}
	return Validator{
		Matchers: Matchers{
			Header: headerMatcher,
			Body:   body,
		},
		Response: Response{
			Code: responseStatus,
			Body: responseBody,
		},
	}
}

type Validator struct {
	Matchers Matchers `json:"matchers"`
	Response
}

func (v Validator) Matches(body string) bool {
	return v.Matchers.Body == body
}

type Matchers struct {
	Header map[string]string `json:"header"`
	Body   string            `json:"body"`
}
