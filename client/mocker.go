package client

import "net"

type Mocker interface {
	HealthCheck() error
	CreateHttpRoute(method, path, responseBody string, responseStatus int, validator ...Validator) (HttpRoute, error)
	CreateWebSocketRoute(path string) (WebSocketRoute, error)
	GetHttpRoutes() ([]HttpRouteDetail, error)
	Addr() net.Addr
	Delete() error
}
