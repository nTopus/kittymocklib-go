# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

default: help

get-deps: ## Install projects dependecies with Go Module
	go mod tidy

clean: ## Clean build project
	@test ! -e bin/${BIN_NAME} || rm bin/${BIN_NAME}

test:  ## Run project tests
	mkdir -p ./.cover
	go test -race -coverpkg= ./... -coverprofile=./.cover/cover.out
	go tool cover -html=./.cover/cover.out -o ./.cover/cover.html